package messaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import entity.DVD;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import org.apache.commons.lang3.SerializationUtils;

public class MailReceiver {

    private static final String EXCHANGE_NAME = "insert";

    private MailService mailService;
    private List<String> clients;

    private MailReceiver() throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        mailService = new MailService("ralucarodicasechel@gmail.com", "Acular1507@");
        clients = new ArrayList<String>();
        clients.add("ralu.rodi@yahoo.com");

        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        System.out.println("MailConsumer waiting for messages");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {

                DVD dvd = SerializationUtils.deserialize(body);

                System.out.println("Received '" + dvd.toString() + "'");

                for (String client : clients) {
                    mailService.sendMail(client, "", dvd.toString());
                    System.out.println(client + " " + dvd);
                }
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

    public static void main(String[] args) throws IOException, TimeoutException {

        new MailReceiver();

    }

}
