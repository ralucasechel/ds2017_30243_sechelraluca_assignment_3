package entity;

import lombok.*;

import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DVD implements Serializable {

    private static final long serialVersionUID = 1L;
    private String title;
    private int year;
    private int price;


}
