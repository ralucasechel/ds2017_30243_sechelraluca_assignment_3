package sender;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang3.SerializationUtils;

public class Sender {
    private static final String EXCHANGE_NAME = "insert";

    private Channel channel;
    private Connection connection;

    public Sender() throws IOException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try {
            connection = factory.newConnection();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        channel.queueDeclare("insert", false, false, false, null);
    }

    public void sendMessage(Serializable object) throws IOException {
        channel.basicPublish(EXCHANGE_NAME, "", null, SerializationUtils.serialize(object));
    }
}